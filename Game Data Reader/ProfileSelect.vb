﻿Public Class ProfileSelect

    Public Property utftest As UTF8EncodingExample

    Private Property profileDir As String
    Dim directories As New Dictionary(Of String, String)

    Private Sub ProfileSelect_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        directories.Clear()
        ListBox1.Items.Clear()
        profileDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Euro Truck Simulator 2\profiles"
        For Each Dir As String In System.IO.Directory.GetDirectories(profileDir)
            utftest = New UTF8EncodingExample
            Dim dirInfo As New System.IO.DirectoryInfo(Dir)
            Console.WriteLine(dirInfo.Name)
            utftest.Main(dirInfo.Name)
            ListBox1.Items.Add(utftest.com)
            directories.Add(utftest.com, dirInfo.Name)
        Next
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If directories.ContainsKey(ListBox1.SelectedItem) Then
            ' Write value of the key.
            Dim directory As String = directories.Item(ListBox1.SelectedItem)
            My.Settings.lastloaded = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Euro Truck Simulator 2\profiles\" + directory
            Me.Close()
        End If
    End Sub
End Class