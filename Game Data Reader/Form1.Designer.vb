﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bank_balance = New System.Windows.Forms.Label()
        Me.bankbalance = New System.Windows.Forms.TextBox()
        Me.loans = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.overdraft = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.base_city = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.experience = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.current_clean_distance = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.best_clean_distance = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.dist_last_dam = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.best_dist_last_dam = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.dist_last_vio = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.best_dist_vio = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.max_loan = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.emergency_calls = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.last_city = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.undamaged_cargo_row = New System.Windows.Forms.TextBox()
        Me.red_light_fines = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.FileSystemWatcher1 = New System.IO.FileSystemWatcher()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.passwordfield = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.usernamefield = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button1.Location = New System.Drawing.Point(6, 6)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(563, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Select Profile"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Control
        Me.Label1.Location = New System.Drawing.Point(6, 69)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Bank Balance:"
        '
        'bank_balance
        '
        Me.bank_balance.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.bank_balance.AutoSize = True
        Me.bank_balance.Location = New System.Drawing.Point(881, 67)
        Me.bank_balance.Name = "bank_balance"
        Me.bank_balance.Size = New System.Drawing.Size(0, 13)
        Me.bank_balance.TabIndex = 2
        '
        'bankbalance
        '
        Me.bankbalance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.bankbalance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.bankbalance.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.bankbalance.Location = New System.Drawing.Point(147, 66)
        Me.bankbalance.Name = "bankbalance"
        Me.bankbalance.ReadOnly = True
        Me.bankbalance.Size = New System.Drawing.Size(119, 20)
        Me.bankbalance.TabIndex = 3
        '
        'loans
        '
        Me.loans.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.loans.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.loans.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.loans.Location = New System.Drawing.Point(147, 92)
        Me.loans.Name = "loans"
        Me.loans.ReadOnly = True
        Me.loans.Size = New System.Drawing.Size(119, 20)
        Me.loans.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label2.ForeColor = System.Drawing.SystemColors.Control
        Me.Label2.Location = New System.Drawing.Point(6, 95)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Loans:"
        '
        'overdraft
        '
        Me.overdraft.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.overdraft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.overdraft.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.overdraft.Location = New System.Drawing.Point(147, 118)
        Me.overdraft.Name = "overdraft"
        Me.overdraft.ReadOnly = True
        Me.overdraft.Size = New System.Drawing.Size(119, 20)
        Me.overdraft.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Control
        Me.Label3.Location = New System.Drawing.Point(6, 121)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Overdraft:"
        '
        'base_city
        '
        Me.base_city.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.base_city.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.base_city.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.base_city.Location = New System.Drawing.Point(147, 144)
        Me.base_city.Name = "base_city"
        Me.base_city.ReadOnly = True
        Me.base_city.Size = New System.Drawing.Size(119, 20)
        Me.base_city.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label4.ForeColor = System.Drawing.SystemColors.Control
        Me.Label4.Location = New System.Drawing.Point(6, 147)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Base City:"
        '
        'experience
        '
        Me.experience.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.experience.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.experience.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.experience.Location = New System.Drawing.Point(147, 170)
        Me.experience.Name = "experience"
        Me.experience.ReadOnly = True
        Me.experience.Size = New System.Drawing.Size(119, 20)
        Me.experience.TabIndex = 11
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label5.ForeColor = System.Drawing.SystemColors.Control
        Me.Label5.Location = New System.Drawing.Point(6, 173)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(95, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Experience Points:"
        '
        'current_clean_distance
        '
        Me.current_clean_distance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.current_clean_distance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.current_clean_distance.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.current_clean_distance.Location = New System.Drawing.Point(147, 195)
        Me.current_clean_distance.Name = "current_clean_distance"
        Me.current_clean_distance.ReadOnly = True
        Me.current_clean_distance.Size = New System.Drawing.Size(119, 20)
        Me.current_clean_distance.TabIndex = 13
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Control
        Me.Label6.Location = New System.Drawing.Point(6, 198)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Current Clean Distance:"
        '
        'best_clean_distance
        '
        Me.best_clean_distance.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.best_clean_distance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.best_clean_distance.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.best_clean_distance.Location = New System.Drawing.Point(147, 221)
        Me.best_clean_distance.Name = "best_clean_distance"
        Me.best_clean_distance.ReadOnly = True
        Me.best_clean_distance.Size = New System.Drawing.Size(119, 20)
        Me.best_clean_distance.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Control
        Me.Label7.Location = New System.Drawing.Point(6, 224)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(106, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Best Clean Distance:"
        '
        'dist_last_dam
        '
        Me.dist_last_dam.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dist_last_dam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dist_last_dam.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.dist_last_dam.Location = New System.Drawing.Point(182, 247)
        Me.dist_last_dam.Name = "dist_last_dam"
        Me.dist_last_dam.ReadOnly = True
        Me.dist_last_dam.Size = New System.Drawing.Size(84, 20)
        Me.dist_last_dam.TabIndex = 17
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label8.ForeColor = System.Drawing.SystemColors.Control
        Me.Label8.Location = New System.Drawing.Point(6, 250)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(170, 13)
        Me.Label8.TabIndex = 16
        Me.Label8.Text = "Distance since last cargo damage:"
        '
        'best_dist_last_dam
        '
        Me.best_dist_last_dam.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.best_dist_last_dam.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.best_dist_last_dam.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.best_dist_last_dam.Location = New System.Drawing.Point(215, 273)
        Me.best_dist_last_dam.Name = "best_dist_last_dam"
        Me.best_dist_last_dam.ReadOnly = True
        Me.best_dist_last_dam.Size = New System.Drawing.Size(51, 20)
        Me.best_dist_last_dam.TabIndex = 19
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label9.ForeColor = System.Drawing.SystemColors.Control
        Me.Label9.Location = New System.Drawing.Point(6, 276)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(203, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Best Distance Since Last Cargo Damage:"
        '
        'dist_last_vio
        '
        Me.dist_last_vio.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.dist_last_vio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dist_last_vio.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.dist_last_vio.Location = New System.Drawing.Point(147, 299)
        Me.dist_last_vio.Name = "dist_last_vio"
        Me.dist_last_vio.ReadOnly = True
        Me.dist_last_vio.Size = New System.Drawing.Size(119, 20)
        Me.dist_last_vio.TabIndex = 21
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label10.ForeColor = System.Drawing.SystemColors.Control
        Me.Label10.Location = New System.Drawing.Point(6, 302)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(141, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Distance since last violation:"
        '
        'best_dist_vio
        '
        Me.best_dist_vio.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.best_dist_vio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.best_dist_vio.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.best_dist_vio.Location = New System.Drawing.Point(177, 325)
        Me.best_dist_vio.Name = "best_dist_vio"
        Me.best_dist_vio.ReadOnly = True
        Me.best_dist_vio.Size = New System.Drawing.Size(89, 20)
        Me.best_dist_vio.TabIndex = 23
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label11.ForeColor = System.Drawing.SystemColors.Control
        Me.Label11.Location = New System.Drawing.Point(6, 328)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(165, 13)
        Me.Label11.TabIndex = 22
        Me.Label11.Text = "Best Distance since last violation:"
        '
        'max_loan
        '
        Me.max_loan.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.max_loan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.max_loan.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.max_loan.Location = New System.Drawing.Point(177, 351)
        Me.max_loan.Name = "max_loan"
        Me.max_loan.ReadOnly = True
        Me.max_loan.Size = New System.Drawing.Size(89, 20)
        Me.max_loan.TabIndex = 25
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.Label12.ForeColor = System.Drawing.SystemColors.Control
        Me.Label12.Location = New System.Drawing.Point(6, 354)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(57, 13)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Max Loan:"
        '
        'emergency_calls
        '
        Me.emergency_calls.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.emergency_calls.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emergency_calls.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.emergency_calls.Location = New System.Drawing.Point(446, 67)
        Me.emergency_calls.Name = "emergency_calls"
        Me.emergency_calls.ReadOnly = True
        Me.emergency_calls.Size = New System.Drawing.Size(121, 20)
        Me.emergency_calls.TabIndex = 27
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label13.Location = New System.Drawing.Point(305, 70)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 13)
        Me.Label13.TabIndex = 26
        Me.Label13.Text = "Emergency Calls:"
        '
        'last_city
        '
        Me.last_city.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.last_city.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.last_city.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.last_city.Location = New System.Drawing.Point(446, 93)
        Me.last_city.Name = "last_city"
        Me.last_city.ReadOnly = True
        Me.last_city.Size = New System.Drawing.Size(121, 20)
        Me.last_city.TabIndex = 29
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label14.Location = New System.Drawing.Point(305, 96)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(84, 13)
        Me.Label14.TabIndex = 28
        Me.Label14.Text = "Last City Visited:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label15.Location = New System.Drawing.Point(305, 122)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(124, 13)
        Me.Label15.TabIndex = 30
        Me.Label15.Text = "Undamaged Cargo Row:"
        '
        'undamaged_cargo_row
        '
        Me.undamaged_cargo_row.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.undamaged_cargo_row.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.undamaged_cargo_row.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.undamaged_cargo_row.Location = New System.Drawing.Point(446, 119)
        Me.undamaged_cargo_row.Name = "undamaged_cargo_row"
        Me.undamaged_cargo_row.ReadOnly = True
        Me.undamaged_cargo_row.Size = New System.Drawing.Size(121, 20)
        Me.undamaged_cargo_row.TabIndex = 31
        '
        'red_light_fines
        '
        Me.red_light_fines.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.red_light_fines.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.red_light_fines.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.red_light_fines.Location = New System.Drawing.Point(446, 145)
        Me.red_light_fines.Name = "red_light_fines"
        Me.red_light_fines.ReadOnly = True
        Me.red_light_fines.Size = New System.Drawing.Size(121, 20)
        Me.red_light_fines.TabIndex = 33
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label16.Location = New System.Drawing.Point(305, 148)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(84, 13)
        Me.Label16.TabIndex = 32
        Me.Label16.Text = "Red Light Fines:"
        '
        'FileSystemWatcher1
        '
        Me.FileSystemWatcher1.EnableRaisingEvents = True
        Me.FileSystemWatcher1.IncludeSubdirectories = True
        Me.FileSystemWatcher1.SynchronizingObject = Me
        '
        'Timer1
        '
        Me.Timer1.Interval = 5000
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.TextBox3.Location = New System.Drawing.Point(445, 171)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(122, 20)
        Me.TextBox3.TabIndex = 43
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label18.Location = New System.Drawing.Point(304, 174)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(66, 13)
        Me.Label18.TabIndex = 42
        Me.Label18.Text = "Last Loaded"
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(583, 502)
        Me.TabControl1.TabIndex = 44
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.Button5)
        Me.TabPage1.Controls.Add(Me.Button4)
        Me.TabPage1.Controls.Add(Me.Button1)
        Me.TabPage1.Controls.Add(Me.TextBox3)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.bank_balance)
        Me.TabPage1.Controls.Add(Me.bankbalance)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.loans)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.overdraft)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.red_light_fines)
        Me.TabPage1.Controls.Add(Me.base_city)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.undamaged_cargo_row)
        Me.TabPage1.Controls.Add(Me.experience)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.last_city)
        Me.TabPage1.Controls.Add(Me.current_clean_distance)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.emergency_calls)
        Me.TabPage1.Controls.Add(Me.best_clean_distance)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.max_loan)
        Me.TabPage1.Controls.Add(Me.dist_last_dam)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.best_dist_vio)
        Me.TabPage1.Controls.Add(Me.best_dist_last_dam)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.dist_last_vio)
        Me.TabPage1.Location = New System.Drawing.Point(4, 25)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(575, 473)
        Me.TabPage1.TabIndex = 1
        Me.TabPage1.Text = "Profile"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button4.Location = New System.Drawing.Point(6, 35)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(563, 23)
        Me.Button4.TabIndex = 44
        Me.Button4.Text = "Manually Update"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.Button3)
        Me.TabPage2.Controls.Add(Me.passwordfield)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.usernamefield)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.TextBox1)
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 25)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(575, 473)
        Me.TabPage2.TabIndex = 2
        Me.TabPage2.Text = "Settings"
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button3.Location = New System.Drawing.Point(8, 100)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(559, 23)
        Me.Button3.TabIndex = 53
        Me.Button3.Text = "Login"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'passwordfield
        '
        Me.passwordfield.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.passwordfield.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.passwordfield.Location = New System.Drawing.Point(70, 48)
        Me.passwordfield.Name = "passwordfield"
        Me.passwordfield.PasswordChar = Global.Microsoft.VisualBasic.ChrW(8226)
        Me.passwordfield.Size = New System.Drawing.Size(497, 20)
        Me.passwordfield.TabIndex = 52
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label19.Location = New System.Drawing.Point(8, 50)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 13)
        Me.Label19.TabIndex = 51
        Me.Label19.Text = "Password:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label20.Location = New System.Drawing.Point(8, 24)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(58, 13)
        Me.Label20.TabIndex = 50
        Me.Label20.Text = "Username:"
        '
        'usernamefield
        '
        Me.usernamefield.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.usernamefield.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.usernamefield.Location = New System.Drawing.Point(72, 22)
        Me.usernamefield.Name = "usernamefield"
        Me.usernamefield.Size = New System.Drawing.Size(495, 20)
        Me.usernamefield.TabIndex = 49
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label17.Location = New System.Drawing.Point(8, 77)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(65, 13)
        Me.Label17.TabIndex = 48
        Me.Label17.Text = "Unique Key:"
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Location = New System.Drawing.Point(79, 74)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(488, 20)
        Me.TextBox1.TabIndex = 47
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button2.Location = New System.Drawing.Point(8, 129)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(559, 23)
        Me.Button2.TabIndex = 46
        Me.Button2.Text = "Send Data to Server"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button5.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button5.Location = New System.Drawing.Point(8, 442)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(561, 23)
        Me.Button5.TabIndex = 47
        Me.Button5.Text = "Send Data to Server"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer), CType(CType(38, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(583, 502)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Logistics Mod Truck Management System"
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bank_balance As System.Windows.Forms.Label
    Friend WithEvents bankbalance As System.Windows.Forms.TextBox
    Friend WithEvents loans As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents overdraft As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents base_city As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents experience As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents current_clean_distance As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents best_clean_distance As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dist_last_dam As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents best_dist_last_dam As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dist_last_vio As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents best_dist_vio As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents max_loan As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents emergency_calls As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents last_city As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents undamaged_cargo_row As System.Windows.Forms.TextBox
    Friend WithEvents red_light_fines As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents FileSystemWatcher1 As System.IO.FileSystemWatcher
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents usernamefield As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents passwordfield As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button

End Class
