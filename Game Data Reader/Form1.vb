﻿
Imports System.Text
Imports System.IO
Imports System.Net
Imports Microsoft.VisualBasic.Strings
Imports System.Xml

Public Class Form1

    Private Property currentfile As String

    Private Property money_account As String

    Private Property bankbalance_var As String

    Private Property loans_var As String

    Private Property overdraft_var As String

    Private Property max_loan_var As String

    Private Property base_city_var As String

    Private Property experience_var As String

    Private Property current_clean_distance_var As String

    Private Property best_clean_distance_var As String

    Private Property dist_last_dam_var As String

    Private Property best_dist_last_dam_var As String

    Private Property dist_last_vio_var As String

    Private Property best_dist_vio_var As String

    Private Property emergency_calls_var As String

    Private Property last_city_var As String

    Private Property undamaged_cargo_row_var As String

    Private Property red_light_fines_var As String

    Private Property post_data As String

    Private Property issending As Integer = 0

    Private Property currentfolder

    Private Property utftest As UTF8EncodingExample

    Dim userName As String

    Dim uniqueKey As String

    Private Sub ProfileSelect_Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        TextBox3.Text = My.Settings.lastloaded
        If My.Settings.lastloaded <> "" Then
            'Console.WriteLine("folder selected")
            currentfolder = My.Settings.lastloaded
            'FileSystemWatcher1.Path = currentfolder
            currentfile = currentfolder + "\save\autosave\game.sii"

            Process.Start("CMD", "/C java -jar scsc.jar """ + currentfile + """")

            'Start of profile name conversion
            'utftest = New UTF8EncodingExample
            'For Each Dir As String In System.IO.Directory.GetDirectories(FolderBrowserDialog1.SelectedPath)
            ' Dim dirInfo As New System.IO.DirectoryInfo(Dir)
            ' UTF8EncodingExample.Main(dirInfo.Name)
            ' Console.Write(UTF8EncodingExample.Length)
            ' Next

            'End of profile name conversion
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'FolderBrowserDialog1.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Euro Truck Simulator 2\profiles"
        'folderBrowserDialog1.ShowDialog() = DialogResult.OK
        ProfileSelect.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        'If issending = 1 Then
        '    issending = 0
        'ElseIf issending = 0 Then
        '    issending = 1
        'End If
    End Sub

    Private Function txtCall() As Object
        Throw New NotImplementedException
    End Function

    'Private Sub FileSystemWatcher1_Changed(sender As Object, e As FileSystemEventArgs) Handles FileSystemWatcher1.Changed
    '    If issending = 1 Then
    '        post_data = "bank_balance=" + bankbalance_var + "&loans=" + loans_var + "&overdraft=" + overdraft_var + "&base_city=" + base_city_var + "&experience=" + experience_var + "&current_clean_distance=" + current_clean_distance_var + "&best_clean_distance=" + best_clean_distance_var + "&dist_last_vio=" + dist_last_vio_var
    '        ' Create a request using a URL that can receive a post. 
    '        Dim request As WebRequest = WebRequest.Create("http://localhost/managment-console/processing/post_preprocessed.php")
    '        ' Set the Method property of the request to POST.
    '        request.Method = "POST"
    '        ' Create POST data and convert it to a byte array.
    '        Dim postData As String = post_data
    '        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
    '        ' Set the ContentType property of the WebRequest.
    '        request.ContentType = "application/x-www-form-urlencoded"
    '        ' Set the ContentLength property of the WebRequest.
    '        request.ContentLength = byteArray.Length
    '        ' Get the request stream.
    '        Dim dataStream As Stream = request.GetRequestStream()
    '        ' Write the data to the request stream.
    '        dataStream.Write(byteArray, 0, byteArray.Length)
    '        ' Close the Stream object.
    '        dataStream.Close()
    '        ' Get the response.
    '        Dim response As WebResponse = request.GetResponse()
    '        ' Display the status.
    '        Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
    '        ' Get the stream containing content returned by the server.
    '        dataStream = response.GetResponseStream()
    '        ' Open the stream using a StreamReader for easy access.
    '        Dim reader As New StreamReader(dataStream)
    '        ' Read the content.
    '        Dim responseFromServer As String = reader.ReadToEnd()
    '        ' Display the content.
    '        Console.WriteLine(responseFromServer)
    '        ' Clean up the streams.
    '        reader.Close()
    '        dataStream.Close()
    '        response.Close()
    '        'MsgBox("request sent")
    '    End If

    'End Sub

    Private Sub FileSystemWatcher1_Changed(sender As Object, e As FileSystemEventArgs) Handles FileSystemWatcher1.Changed
        Using sr As New StreamReader(currentfile)
            While Not sr.EndOfStream()
                Dim line As String = sr.ReadLine
                If line.StartsWith(" money_account:") Then
                    Dim Parts() As String = Split(line, ":")
                    bankbalance.Text = Parts(1)
                    bankbalance_var = Parts(1)
                End If
                If line.StartsWith(" loans:") Then
                    Dim Parts() As String = Split(line, ":")
                    loans.Text = Parts(1)
                    loans_var = Parts(1)
                End If
                If line.StartsWith(" overdraft:") Then
                    Dim Parts() As String = Split(line, ":")
                    overdraft.Text = Parts(1)
                    overdraft_var = Parts(1)
                End If
                If line.StartsWith(" loan_limit:") Then
                    Dim Parts() As String = Split(line, ":")
                    max_loan.Text = Parts(1)
                    max_loan_var = Parts(1)
                End If
                If line.StartsWith(" hq_city:") Then
                    Dim Parts() As String = Split(line, ":")
                    base_city.Text = Parts(1)
                    base_city_var = Parts(1)
                End If
                If line.StartsWith(" experience_points:") Then
                    Dim Parts() As String = Split(line, ":")
                    experience.Text = Parts(1)
                    experience_var = Parts(1)
                End If
                If line.StartsWith(" clean_distance_counter:") Then
                    Dim Parts() As String = Split(line, ":")
                    current_clean_distance.Text = Parts(1)
                    current_clean_distance_var = Parts(1)
                End If
                If line.StartsWith(" clean_distance_max:") Then
                    Dim Parts() As String = Split(line, ":")
                    best_clean_distance.Text = Parts(1)
                    best_clean_distance_var = Parts(1)
                End If
                If line.StartsWith(" no_cargo_damage_distance_counter:") Then
                    Dim Parts() As String = Split(line, ":")
                    dist_last_dam.Text = Parts(1)
                    dist_last_dam_var = Parts(1)
                End If
                If line.StartsWith(" no_cargo_damage_distance_max:") Then
                    Dim Parts() As String = Split(line, ":")
                    best_dist_last_dam.Text = Parts(1)
                    best_dist_last_dam_var = Parts(1)
                End If
                If line.StartsWith(" no_violation_distance_counter:") Then
                    Dim Parts() As String = Split(line, ":")
                    dist_last_vio.Text = Parts(1)
                    dist_last_vio_var = Parts(1)
                End If
                If line.StartsWith(" no_violation_distance_max:") Then
                    Dim Parts() As String = Split(line, ":")
                    best_dist_vio.Text = Parts(1)
                    best_dist_vio_var = Parts(1)
                End If
                If line.StartsWith(" loan_limit:") Then
                    Dim Parts() As String = Split(line, ":")
                    max_loan.Text = Parts(1)
                    max_loan_var = Parts(1)
                End If
                If line.StartsWith(" emergency_call_count:") Then
                    Dim Parts() As String = Split(line, ":")
                    emergency_calls.Text = Parts(1)
                    emergency_calls_var = Parts(1)
                End If
                If line.StartsWith(" last_visited_city:") Then
                    Dim Parts() As String = Split(line, ":")
                    last_city.Text = Parts(1)
                    last_city_var = Parts(1)
                End If
                If line.StartsWith(" undamaged_cargo_row:") Then
                    Dim Parts() As String = Split(line, ":")
                    undamaged_cargo_row.Text = Parts(1)
                    undamaged_cargo_row_var = Parts(1)
                End If
                If line.StartsWith(" red_light_fine_count:") Then
                    Dim Parts() As String = Split(line, ":")
                    red_light_fines.Text = Parts(1)
                    red_light_fines_var = Parts(1)
                End If
            End While
        End Using

        'If issending = 1 Then
        '    post_data = "bank_balance=" + bankbalance_var + "&loans=" + loans_var + "&overdraft=" + overdraft_var + "&base_city=" + base_city_var + "&experience=" + experience_var + "&current_clean_distance=" + current_clean_distance_var + "&best_clean_distance=" + best_clean_distance_var + "&dist_last_vio=" + dist_last_vio_var
        '    ' Create a request using a URL that can receive a post. 
        '    Dim request As WebRequest = WebRequest.Create("http://localhost/managment-console/processing/post_preprocessed.php")
        '    ' Set the Method property of the request to POST.
        '    request.Method = "POST"
        '    ' Create POST data and convert it to a byte array.
        '    Dim postData As String = post_data
        '    Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
        '    ' Set the ContentType property of the WebRequest.
        '    request.ContentType = "application/x-www-form-urlencoded"
        '    ' Set the ContentLength property of the WebRequest.
        '    request.ContentLength = byteArray.Length
        '    ' Get the request stream.
        '    Dim dataStream As Stream = request.GetRequestStream()
        '    ' Write the data to the request stream.
        '    dataStream.Write(byteArray, 0, byteArray.Length)
        '    ' Close the Stream object.
        '    dataStream.Close()
        '    ' Get the response.
        '    Dim response As WebResponse = request.GetResponse()
        '    ' Display the status.
        '    Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
        '    ' Get the stream containing content returned by the server.
        '    dataStream = response.GetResponseStream()
        '    ' Open the stream using a StreamReader for easy access.
        '    Dim reader As New StreamReader(dataStream)
        '    ' Read the content.
        '    Dim responseFromServer As String = reader.ReadToEnd()
        '    ' Display the content.
        '    Console.WriteLine(responseFromServer)
        '    ' Clean up the streams.
        '    reader.Close()
        '    dataStream.Close()
        '    response.Close()
        '    'MsgBox("request sent")
        'End If

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBox3.Text = My.Settings.lastloaded

        usernamefield.Text = My.Settings.username.ToString
        passwordfield.Text = My.Settings.password.ToString
        TextBox1.Text = My.Settings.uniquekey.ToString

        userName = usernamefield.Text
        uniqueKey = TextBox1.Text
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub red_light_fines_TextChanged(sender As Object, e As EventArgs) Handles red_light_fines.TextChanged

    End Sub

    Private Sub undamaged_cargo_row_TextChanged(sender As Object, e As EventArgs) Handles undamaged_cargo_row.TextChanged

    End Sub

    Private Sub last_city_TextChanged(sender As Object, e As EventArgs) Handles last_city.TextChanged

    End Sub

    Private Sub emergency_calls_TextChanged(sender As Object, e As EventArgs) Handles emergency_calls.TextChanged

    End Sub

    Private Sub Button3_Click_1(sender As Object, e As EventArgs)
        userName = usernamefield.Text
        uniqueKey = TextBox1.Text
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Using sr As New StreamReader(currentfile)
            While Not sr.EndOfStream()
                Dim line As String = sr.ReadLine
                If line.StartsWith(" money_account:") Then
                    Dim Parts() As String = Split(line, ":")
                    bankbalance.Text = Parts(1)
                    bankbalance_var = Parts(1)
                End If
                If line.StartsWith(" loans:") Then
                    Dim Parts() As String = Split(line, ":")
                    loans.Text = Parts(1)
                    loans_var = Parts(1)
                End If
                If line.StartsWith(" overdraft:") Then
                    Dim Parts() As String = Split(line, ":")
                    overdraft.Text = Parts(1)
                    overdraft_var = Parts(1)
                End If
                If line.StartsWith(" loan_limit:") Then
                    Dim Parts() As String = Split(line, ":")
                    max_loan.Text = Parts(1)
                    max_loan_var = Parts(1)
                End If
                If line.StartsWith(" hq_city:") Then
                    Dim Parts() As String = Split(line, ":")
                    base_city.Text = Parts(1)
                    base_city_var = Parts(1)
                End If
                If line.StartsWith(" experience_points:") Then
                    Dim Parts() As String = Split(line, ":")
                    experience.Text = Parts(1)
                    experience_var = Parts(1)
                End If
                If line.StartsWith(" clean_distance_counter:") Then
                    Dim Parts() As String = Split(line, ":")
                    current_clean_distance.Text = Parts(1)
                    current_clean_distance_var = Parts(1)
                End If
                If line.StartsWith(" clean_distance_max:") Then
                    Dim Parts() As String = Split(line, ":")
                    best_clean_distance.Text = Parts(1)
                    best_clean_distance_var = Parts(1)
                End If
                If line.StartsWith(" no_cargo_damage_distance_counter:") Then
                    Dim Parts() As String = Split(line, ":")
                    dist_last_dam.Text = Parts(1)
                    dist_last_dam_var = Parts(1)
                End If
                If line.StartsWith(" no_cargo_damage_distance_max:") Then
                    Dim Parts() As String = Split(line, ":")
                    best_dist_last_dam.Text = Parts(1)
                    best_dist_last_dam_var = Parts(1)
                End If
                If line.StartsWith(" no_violation_distance_counter:") Then
                    Dim Parts() As String = Split(line, ":")
                    dist_last_vio.Text = Parts(1)
                    dist_last_vio_var = Parts(1)
                End If
                If line.StartsWith(" no_violation_distance_max:") Then
                    Dim Parts() As String = Split(line, ":")
                    best_dist_vio.Text = Parts(1)
                    best_dist_vio_var = Parts(1)
                End If
                If line.StartsWith(" loan_limit:") Then
                    Dim Parts() As String = Split(line, ":")
                    max_loan.Text = Parts(1)
                    max_loan_var = Parts(1)
                End If
                If line.StartsWith(" emergency_call_count:") Then
                    Dim Parts() As String = Split(line, ":")
                    emergency_calls.Text = Parts(1)
                    emergency_calls_var = Parts(1)
                End If
                If line.StartsWith(" last_visited_city:") Then
                    Dim Parts() As String = Split(line, ":")
                    last_city.Text = Parts(1)
                    last_city_var = Parts(1)
                End If
                If line.StartsWith(" undamaged_cargo_row:") Then
                    Dim Parts() As String = Split(line, ":")
                    undamaged_cargo_row.Text = Parts(1)
                    undamaged_cargo_row_var = Parts(1)
                End If
                If line.StartsWith(" red_light_fine_count:") Then
                    Dim Parts() As String = Split(line, ":")
                    red_light_fines.Text = Parts(1)
                    red_light_fines_var = Parts(1)
                End If
            End While
        End Using
    End Sub

    Private Sub Button3_Click_2(sender As Object, e As EventArgs) Handles Button3.Click
        My.Settings.username = usernamefield.Text
        My.Settings.password = passwordfield.Text
        My.Settings.uniquekey = TextBox1.Text
    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub
End Class
Public Class UTF8EncodingExample
    Public com As String = ""
    Shared st As String = ""
    Public Sub Main(st)
        For x = 0 To st.Length - 1 Step 2
            Dim k As String = st.Substring(x, 2)
            com &= System.Convert.ToChar(System.Convert.ToUInt32(k, 16)).ToString()
        Next
    End Sub
End Class
